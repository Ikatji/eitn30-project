# EITN30 Project
By group 2

## Description
LongGé implementation, with an object security focus.

## Usage
To set up forwarding on the base station unit, use the bash file src/commandes.sh.

To start up the LongGé system, run src/BS.py on the base station unit and src/MS.py on the mobile station unit.
