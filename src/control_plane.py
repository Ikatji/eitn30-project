import math
from tuntap import TunTap
import os
#from SO import Agent

frame_size = 32*8   # size in bits
header_size = 8     # size in bits
CRC = 8             # size in bits

def parse_frame(frame):

    return 0


def reassemble(frame_list):
    packet = ""
    for frame in frame_list: # assuming headers were checked
        #print((frame))
        #print(bin(frame[0]))
     
        CRC_     = (to_binary(frame[-1:]) )
        header_  = (to_binary(frame[-2:-1]))
        payload_ =  (to_binary(frame[:-2]))
 
        #print(frame)
        #print("CRC",CRC_)
        #print("header",header_)
        #print(payload_)
        packet = packet + payload_
        bytes_packets = int(packet, 2).to_bytes((len(packet) + 7) // 8, byteorder='big')
        #print("bytes_packets",bytes_packets)



    return bytes_packets



def encapsulate(ip_packet):
    payload_list = fragment(ip_packet, frame_size)
    frame_list = []

    header = "00000000" # data packet, first
    CRC = to_binary(int(len(payload_list[0])).to_bytes(1,byteorder='big'))
    frame_list.append( payload_list[0]+ header+CRC)
    #print("first header")
    #print(CRC)
    #print(frame_list)
    
    header = "00000001" # data packet, not final
    for n in range(len(payload_list)-2):
        CRC = to_binary(int(len(payload_list[n+1])).to_bytes(1,byteorder='big'))
        frame_list.append( payload_list[n+1] + header + CRC)
        #print("secande header")
        #print(frame_list)

    header = "00000010" # data packet, final
    CRC = to_binary(int(len(payload_list[len(payload_list)-1])).to_bytes(1,byteorder='big'))
    frame_list.append( payload_list[len(payload_list)-1] + header+CRC)
    #print("thired header")
    #print(CRC)
    #print(frame_list[1])



    for n, frame in enumerate(frame_list):
        frame_list[n] = int(frame, 2).to_bytes((len(frame) + 7) // 8, byteorder='big')
        # print("byte_frame #{}:".format(n), frame_list[n], len(frame_list[n]))
    return frame_list

def fragment(ip_packet, frame_size):
    payload_size = frame_size - (header_size+ CRC )   # size in bits
    payload_count =  math.ceil(len(ip_packet)*8/(payload_size))
    # print("payload_count = {}, payload_size = {}".format(payload_count, payload_size))
    packet_binary = to_binary(ip_packet)
    # print("packet_binary =", packet_binary, len(packet_binary)/8)
    payload_list = []
    for n in range(payload_count):
        payload_list.append(packet_binary[n*payload_size:(n+1)*payload_size])
        # print("length:", len(payload_list[n]))
    return payload_list

# Reinterprets a bytes object into a string in binary format.
def to_binary(message_bytes):
    message_binary = ""
    for b in range(len(message_bytes)):
        binary = bin(message_bytes[b])[2:]
        binary = "0"*(8-len(binary)) + binary
        # print("Byte index {}:".format(b), binary)
        message_binary = message_binary + binary
    return message_binary


def init_interface():
    tun = TunTap(nic_type="Tun", nic_name="tun0")
    tun.config(ip="192.168.1.11", mask="255.255.255.0", gateway="192.168.2.2")
    return tun

def test1():
    tun = init_interface()

    os.system("ping -c 3 -I tun0 130.235.200.117")
    buffer = ""
    get_next = "n"
    while get_next == "n":
        buffer = tun.read()
        print("Packet of length {}:".format(len(buffer)), "\n", buffer, "\n", buffer.hex())
        # to_binary(buffer)
        payload_list = fragment(buffer, frame_size)
        print("Binary payload list:", payload_list, len(payload_list))
        frame_list = encapsulate(buffer)
        print("Frame list:", frame_list, len(frame_list))
        print(bin(frame_list[0][0]))
        print(bin(frame_list[0][1]))
        print(bin(frame_list[1][0]), "\n")
        if len(frame_list) > 2:
            print(bin(frame_list[2][0]))
            print(bin(frame_list[2][1]))
            print(bin(frame_list[2][2]), "\n")
        packet = reassemble(frame_list)
        print("Reassembled packet:", packet)
        get_next = input("\nEnter 'n' to get next packet.\n")
    tun.close()
    print("\n")

def test():
    tun = init_interface()



    tun.close()
    print("\n")

#test1()
