import json
import socket
import random
import sys
from Cryptodome.Hash import HMAC
from Cryptodome.Hash import SHA256
from Cryptodome.Cipher import AES
from Cryptodome import Random
import math

   
    
key = 'Sixteen byte key'
def sign(msg):
        h = HMAC.new(key.encode("utf8"))
        h.update(msg.encode("utf8"))
        digest = h.hexdigest()
        return digest


def verify_signature(msg,other_signature):
        my_signature= sign(msg)
        return my_signature == other_signature


    
def encrypt(msg):
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key.encode("utf8")[:32], AES.MODE_EAX  ,iv)
        encrypted_msg = iv+cipher.encrypt(msg.encode("utf8"))
        return encrypted_msg.hex()


def decrypt(encrypted_msg):
        cipher = AES.new(key.encode("utf8")[:32], AES.MODE_EAX  ,encrypted_msg[0:16])
        msg = cipher.decrypt(encrypted_msg[16:])
        return msg

   


