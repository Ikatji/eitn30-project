from multiprocessing import Process
from circuitpython_nrf24l01.rf24 import RF24
import board
import busio
import digitalio as dio
import time
import struct
import argparse
from random import randint
import numpy as np
from struct import *
import math
from tuntap import TunTap
import os
import control_plane as CP

print(0)

SPI0 = {
    'MOSI':10,#dio.DigitalInOut(board.D10),
    'MISO':9,#dio.DigitalInOut(board.D9),
    'clock':11,#dio.DigitalInOut(board.D11),
    'ce_pin':dio.DigitalInOut(board.D17),
    'csn':dio.DigitalInOut(board.D8),
    }
SPI1 = {
    'MOSI':20,#dio.DigitalInOut(board.D10),
    'MISO':19,#dio.DigitalInOut(board.D9),
    'clock':21,#dio.DigitalInOut(board.D11),
    'ce_pin':dio.DigitalInOut(board.D27),
    'csn':dio.DigitalInOut(board.D18),
    }

print(1)

def tx(nrf, channel, address, count, size, tun):
    print("tx start")
    nrf.open_tx_pipe(address)  # set address of RX node into a TX pipe
    nrf.listen = False
    nrf.channel = channel

    status = []
    #buffer = np.random.bytes(size)

    #start = time.monotonic()
    
    while True:
        #response = os.system("ping -I "+ tun_name +" "+ host)
        buffer = tun.read()
        #if response == 0:
        #   print("The Ping worked successfully!")
        #else:
        #   print("Bad news, NO ping response!")
        
        #print("Enter message to send: ")
        #buffer = input().encode()
        #buffer = np.random.bytes(size)
        message_toSend= CP.encapsulate(buffer)
        print("Incoming:", message_toSend)

       # if len(buffer) > 32 :
        #   new_buff= segment_data(buffer)
         #  print(new_buff)
         #  for i in new_buff:
          #      print("Sending {} bytes: {}".format(len(i), i))
                # use struct.pack to packetize your data
                # into a usable payload

                #buffer = struct.pack("<i", count)
                # 'i' means a single 4 byte int value.
                # '<' means little endian byte order. this may be optional
                 #print("Sending: {} as struct: {}".format(count, buffer))
           #     result = nrf.send(i)
                #_ping_and_prompt()
            #    print('result:',result)
             #   if not result:
              #     print("send() failed or timed out")
                   #print(nrf.print_details())
               #    status.append(False)
            
                #else:
                 # print("send() successful")
                 # status.append(True)
                  # print("timer results despite transmission success")
                  #count -= 1
                  #total_time = time.monotonic() - start

          

        
        print("Sending {} bytes: {}".format(len(message_toSend), message_toSend))
             # use struct.pack to packetize your data
             # into a usable payload

             #buffer = struct.pack("<i", count)
              # 'i' means a single 4 byte int value.
             # '<' means little endian byte order. this may be optional
             #print("Sending: {} as struct: {}".format(count, buffer))
        result = nrf.send(message_toSend,force_retry=50)
        #_ping_and_prompt()
        for r in result:
            if not r:
                print("send() failed or timed out")
                  # print(nrf.what_happened())
                status.append(False)
            else:
                print("send() successful")
                status.append(True)
            # print("timer results despite transmission success")
            #count -= 1
            #total_time = time.monotonic() - start

            
        #print("args.src:", args.src)
        #print("tx_nrf.address:", tx_nrf.address())
            #print('{} successfull transmissions, {} failures, {} bps'.format(sum(status), len(status)-sum(status), size*8*len(status)/total_time))


def rx(nrf, channel, address, count, tun):
    nrf.open_rx_pipe(0, address)
    nrf.listen = True  # put radio into RX mode and power up
    nrf.channel = channel
    
    frame_list = [] # the full frame deliverd from MS 
    print('Rx NRF24L01+ started w/ power {}, SPI freq: {} hz'.format(nrf.pa_level, nrf.spi_frequency))
   
    frame_list = []
    
    
    while True:
        if nrf.update() and nrf.pipe is not None:
           
            rx = nrf.read()  # also clears nrf.irq_dr status flag
            header = int.from_bytes(rx[-2:-1], "big")
            print("Header:", header, "\n", "Frame:", rx)
            
            print("This is RX:",rx)
            if header == 0 and rx not in frame_list :
                frame_list = []
                frame_list.append(rx)
                print("New packet!")
            elif header == 1 and rx not in frame_list:
                frame_list.append(rx)
            elif header == 2 and rx not in frame_list:
                frame_list.append(rx)
                packet = CP.reassemble(frame_list)
                print("Received packet:", packet, "\n", packet.hex())
                tun.write(packet)
            else: print("Error: Invalid header")
            #frame_list= list(set(frame_list))
            #print("all_recived_packets",frame_list)
            

       
        else: time.sleep(1)
    
def segment_data(byte_array):
        size = len(byte_array)
        frame_size= 32
        number_packages= math.ceil(size/frame_size)
       # print("-------------------------------------------------------------")
        print("Number packages to send:",number_packages)
        frames = list()
        for i in range(number_packages):
            offset= i * frame_size
            end = offset + frame_size 
            frames.append(byte_array[offset:end])
        print('frames:',frames)
        return frames

def myping(host):
    response = os.system("ping -c 1 " + host)
    
    if response == 0:
        return True
    else:
        return False

def _ping_and_prompt():
    """transmit 1 payload, wait till irq_pin goes active, print IRQ status
     flags."""
    tx_nrf.ce_pin = 1 # tell the nRF24L01 to prepare sending a single packet
    time.sleep(0.00001) # mandatory 10 microsecond pulse starts transmission
    tx_nrf.ce_pin = 0 # end 10 us pulse; use only 1 buffer from TX FIFO
    while SPI1['ce_pin'].value: # IRQ pin is active when LOW
        pass
    print("IRQ pin went active LOW.")
    tx_nrf.update() # update irq_d? status flags
    print(f"\tirq_ds: {tx_nrf.irq_ds}, irq_dr: {tx_nrf.irq_dr}, irq_df: {tx_nrf.irq_df}")



def tun_interface():
    iface= 'LongG'
    # Createand configurea TUN interface
    tun = TunTap(nic_type="Tun", nic_name="tun0")
    tun.config(ip="192.168.1.11", mask="255.255.255.0", gateway="192.168.2.2")
    
    return tun

def main():
    parser = argparse.ArgumentParser(description='NRF24L01+ 20_tx')
    parser.add_argument('--src', dest='src', type=str, default='meee1', help='NRF24L01+\'s source address')
    parser.add_argument('--dst', dest='dst', type=str, default='meee2', help='NRF24L01+\'s destination address')
    parser.add_argument('--count', dest='cnt', type=int, default=10, help='Number of transmissions')
    parser.add_argument('--size', dest='size', type=int, default=32, help='Packet size')
    parser.add_argument('--txchannel', dest='txchannel', type=int, default=90, help='Tx channel', choices=range(0,125))
    parser.add_argument('--rxchannel', dest='rxchannel', type=int, default=80, help='Rx channel', choices=range(0,125))

    args = parser.parse_args()

    SPI0['spi'] = busio.SPI(**{x: SPI0[x] for x in ['clock', 'MOSI', 'MISO']})
    SPI1['spi'] = busio.SPI(**{x: SPI1[x] for x in ['clock', 'MOSI', 'MISO']})

    # initialize the nRF24L01 on the spi bus object
    # rx_nrf = RF24(**{x: SPI0[x] for x in ['spi', 'csn', 'ce']})
    # tx_nrf = RF24(**{x: SPI1[x] for x in ['spi', 'csn', 'ce']})

    rx_nrf = RF24(SPI0['spi'], SPI0['csn'], SPI0['ce_pin'])
    tx_nrf = RF24(SPI1['spi'], SPI1['csn'], SPI1['ce_pin'])
    
    for nrf in [tx_nrf,rx_nrf]: 
        nrf.data_rate = 1
        nrf.auto_ack = True
        nrf.dynamic_payloads = True
        #nrf.payload_length = 32
        nrf.crc = True
        nrf.ack = 1
        nrf.spi_frequency = 20000000
        nrf.arc = 15

    tun = tun_interface()
    tx_process = Process(target=tx, kwargs={'nrf':tx_nrf, 'address':bytes(args.dst, 'utf-8'), 'count': args.cnt, 'channel': args.txchannel, 'size':args.size, 'tun':tun})
    rx_process = Process(target=rx, kwargs={'nrf':rx_nrf, 'address':bytes(args.src, 'utf-8'), 'count': args.cnt, 'channel': args.rxchannel, 'tun': tun})
   
    rx_process.start()
    tx_process.start()
    time.sleep(1)

    tx_process.join()
    rx_process.join()

if __name__ == "__main__":
  
   main()
   
    
