#! /bin/sh.
 
sudo iptables -F FORWARD
sudo iptables -t nat -F POSTROUTING
sudo iptables -A FORWARD -i tun0 -o eth0 -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o tun0 -j ACCEPT
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
