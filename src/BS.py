from multiprocessing import Process
from circuitpython_nrf24l01.rf24 import RF24
import board
import busio
import digitalio as dio
import time
import struct
import argparse
from random import randint
import numpy as np
from tuntap import TunTap
import control_plane as CP
import timeit

SPI0 = {
    'MOSI':10,#dio.DigitalInOut(board.D10),
    'MISO':9,#dio.DigitalInOut(board.D9),
    'clock':11,#dio.DigitalInOut(board.D11),
    'ce_pin':dio.DigitalInOut(board.D17),
    'csn':dio.DigitalInOut(board.D8),
    }
SPI1 = {
    'MOSI':20,#dio.DigitalInOut(board.D10),
    'MISO':19,#dio.DigitalInOut(board.D9),
    'clock':21,#dio.DigitalInOut(board.D11),
    'ce_pin':dio.DigitalInOut(board.D27),
    'csn':dio.DigitalInOut(board.D18),
    }

def rx(nrf, channel, address, count, tun):
    nrf.open_rx_pipe(0, address)
    nrf.listen = True  # put radio into RX mode and power up
    nrf.channel = channel
    
    frame_list = [] # the full frame deliverd from MS 
    print('Rx NRF24L01+ started w/ power {}, SPI freq: {} hz'.format(nrf.pa_level, nrf.spi_frequency))


    # received = []

    #start_time = None
    #start = time.monotonic()
    frame_list = []
    
    #tun = tun_interface()
    while True:
        start = timeit.default_timer()
        if nrf.update() and nrf.pipe is not None:
            
            # print details about the received packet
            # fetch 1 payload from RX FIFO
            # received.append(nrf.any())
            rx = nrf.read()  # also clears nrf.irq_dr status flag
            header = int.from_bytes(rx[-2:-1], "big")
           # print("Header:", header, "\n", "Frame:", rx)
            # expecting an int, thus the string format '<i'
            # the rx[:4] is just in case dynamic payloads were disabled
            # buffer = struct.unpack("<i", rx[:4])  # [:4] truncates padded 0s
            # print the only item in the resulting tuple from
            # using `struct.unpack()`
            # print("Received: {}, Raw: {}".format(received[0], rx.hex()))
            #start = time.monotonic()
            # count -= 1
            # this will listen indefinitely till count == 0
            #tun.read(rx)
           # print("This is RX:",rx)
            if header == 0 and rx not in frame_list :
                frame_list = []
                frame_list.append(rx)
                print("New packet!")
            elif header == 1 and rx not in frame_list:
                frame_list.append(rx)
            elif header == 2 and rx not in frame_list:
                frame_list.append(rx)
                packet = CP.reassemble(frame_list)
              #  print("Received packet:", packet, "\n", packet.hex())
                print("Received packet!")
                tun.write(packet)
            else: print("Error: Invalid header")
            #frame_list= list(set(frame_list))
            #print("all_recived_packets",frame_list)
            

        #tun.close()
        else: time.sleep(1)
        stop = timeit.default_timer()
        print('Time for this packet: ', stop - start)  
    # total_time = time.monotonic() - start_time
    #print('{} received, {} average, {} bps'.format(len(received), np.mean(received), np.sum(received)*8/total_time))

def tx(nrf, channel, address, count, size, tun):
    print("tx start")
    nrf.open_tx_pipe(address)  # set address of RX node into a TX pipe
    nrf.listen = False
    nrf.channel = channel
    status = []
   # tun = tun_interface("192.168.1.11" ,"255.255.255.0",tun_name)
    while True: 
        buffer = tun.read()
        message_toSend= CP.encapsulate(buffer)
      #  print("Incoming:", message_toSend)
      #  print("Sending {} bytes: {}".format(len(message_toSend), message_toSend))
        
        result = nrf.send(message_toSend,force_retry=50)
    
        for r in result:
            if not r:
                print("send() failed or timed out")
                status.append(False)
            else:
                print("send() successful")
                status.append(True)
          

      

def tun_interface():
    iface= 'LongG'
    # Createand configurea TUN interface
    tun = TunTap(nic_type="Tun", nic_name="tun0")
    tun.config(ip="192.168.1.9", mask="255.255.255.0", gateway="192.168.2.2")
    return tun

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='NRF24L01+ test')
    parser.add_argument('--src', dest='src', type=str, default='meee2', help='NRF24L01+\'s source address')
    parser.add_argument('--dst', dest='dst', type=str, default='meee1', help='NRF24L01+\'s destination address')
    parser.add_argument('--count', dest='cnt', type=int, default=10, help='Number of transmissions')
    parser.add_argument('--size', dest='size', type=int, default=32, help='Packet size')
    parser.add_argument('--txchannel', dest='txchannel', type=int, default=80, help='Tx channel', choices=range(0,125))
    parser.add_argument('--rxchannel', dest='rxchannel', type=int, default=90, help='Rx channel', choices=range(0,125))

    args = parser.parse_args()

    SPI0['spi'] = busio.SPI(**{x: SPI0[x] for x in ['clock', 'MOSI', 'MISO']})
    SPI1['spi'] = busio.SPI(**{x: SPI1[x] for x in ['clock', 'MOSI', 'MISO']})

    # initialize the nRF24L01 on the spi bus object
    # rx_nrf = RF24(**{x: SPI0[x] for x in ['spi', 'csn', 'ce']})
    # tx_nrf = RF24(**{x: SPI1[x] for x in ['spi', 'csn', 'ce']})

    rx_nrf = RF24(SPI0['spi'], SPI0['csn'], SPI0['ce_pin'])
    tx_nrf = RF24(SPI1['spi'], SPI1['csn'], SPI1['ce_pin'])

    for nrf in [rx_nrf, tx_nrf]:
        nrf.data_rate = 1
        nrf.auto_ack = True
        nrf.dynamic_payloads = True
        nrf.payload_length = 32
        nrf.crc = True
        nrf.ack = 1
        nrf.spi_frequency = 2000000
    tun = tun_interface() # tn interface for both sender and reciver 
    rx_process = Process(target=rx, kwargs={'nrf':rx_nrf, 'address':bytes(args.src, 'utf-8'), 'count': args.cnt, 'channel': args.rxchannel, 'tun': tun})
    tx_process = Process(target=tx, kwargs={'nrf':tx_nrf, 'address':bytes(args.dst, 'utf-8'), 'count': args.cnt, 'channel': args.txchannel, 'size':args.size, 'tun': tun})

    print("args.src:", args.src)
    print("rx_nrf.address:", rx_nrf.address())

    rx_process.start()
    time.sleep(1)
    tx_process.start()

    tx_process.join()
    rx_process.join()
