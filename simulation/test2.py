import numpy as np 
import matplotlib.pyplot as plt
import math


class simulation:
    def __init__(self):
        self.packets_in_system = 0
        self.clock = 0.0
        self.mean_of_arrival_1=7.34
        self.mean_of_arrival_2=3.67
        self.mean_of_arrival_3=1.825
        self.mean_of_arrival_4=0.9125
        self.mean_of_service= 2.14
        self.mean_of_service_2= 0.428
        self.mean_of_service_3= 0.642
        self.t_arrival = self.generate_inter_arrival()
        self.t_depart =float('inf')
        self.num_arrivals=0
        self.num_departs =0
        self.full_time =0.0
        
        
        
        
       
    
    def advance_time(self):
        t_event = min(self.t_arrival , self.t_depart)  #take the smaller time between departure and arrival!
        self.full_time += self.packets_in_system*(t_event - self.clock)
        self.clock= t_event
        #print('advance_time', t_event)

        if self.t_arrival <= self.t_depart:
            self.handle_arrival_event()
        else:
            self.handle_depart_event()




        
    def handle_arrival_event(self):
        self.packets_in_system +=1  # put a new packet in the queue service
        self.num_arrivals +=1   # increase number of arrival packets
        if self.packets_in_system <= 1:
            self.t_depart = self.clock + self.generate_service()
        self.t_arrival = self.clock + self.generate_inter_arrival()
        


    def handle_depart_event(self):
        self.packets_in_system -= 1 # remove a packet from the queue after service
        self.num_departs += 1   # increase number of deprtal packets
        if self.packets_in_system > 0:
            self.t_depart = self.clock + self.generate_service()
        else:
            self.t_depart = float('inf') 
        #self.t_arrival = self.clock + self.generate_inter_arrival()  


    def generate_inter_arrival(self):   
        return np.random.exponential(self.mean_of_arrival_4) # lambda = 2 , 2 packets in second (2 mobiles sends at the same time)

    #def generate_service(self): 
     #   return np.random.exponential(0.5149) # mu = 27 , 27 packets in second / if we assume that the packet has the max size 1500 bytes
    
    def generate_service(self): 
        return np.random.exponential(self.mean_of_service)  # mu = 27 , 27 packets in second / if we assume that the packet has the max size 1500 bytes
    
   
#np.random.seed(0)

s = simulation()
#print(np.random.exponential(s.mean_of_arrival_2))
#print(np.random.lognormal(s.mean_of_service))
rho = (1/s.mean_of_arrival_4)/(1/s.mean_of_service)
L_q = (math.pow(rho,2)/(1-rho))
W_q = L_q/s.mean_of_arrival_4
W = W_q + 1/s.mean_of_service
L = W*s.mean_of_arrival_4
for i in  range(1000):
    s.advance_time()
print("New Packet  number : ", i )
print('packages in system:  ',s.packets_in_system)
print('total_num_arrivals',s.num_arrivals)
print('total packages delivered: ',s.num_departs)
print("clock",s.clock)
print("full_time",s.full_time)
print("t_depart", s.t_depart)
print("t_arrival",s.t_arrival)
print("The Rho is:", rho)
print("Number in the Queue:",L_q )
print("Wait in the Queue:",W_q )
print("Wait in the System(W):",W)
print("Number in the System(L)",L)
print("----------------")

#servic_ = range(100,300)
#arrival_ = range(100,5000)
#servis_list=[]
#arrival_list=[]

#for i in servic_:
 #   servis_list.append(i)
    

#for i in arrival_:
    
 #   arrival_list.append(i)
#print(servis_list, arrival_list)
# plot
#plt.figure(figsize=(200,4900))
##plt.plot([100,1000,2000],[1,2,3])

#plt.show()

