import numpy as np
import matplotlib.pyplot as plt

from matplotlib.ticker import NullFormatter  # useful for `logit` scale

# Fixing random state for reproducibility

# make up some data in the interval ]0, 1[
y = np.random.exponential( scale=0.734, size=1000)
y = y[(y > 0) & (y < 2)]
y.sort()
x = np.arange(len(y))


# plot with various axes scales
plt.figure("1 MS")

# linear
plt.subplot(221)
plt.plot(x, y)
plt.yscale('linear')
plt.title('1 MS')
plt.grid(True)

y2 = np.random.exponential( scale=0.367, size=1000)
y2 = y2[(y2 > 0) & (y2 < 2)]
y2.sort()
x2 = np.arange(len(y2))
# plot with various axes scales
#plt.figure("2 MS")
# linear
plt.subplot(222)
plt.plot(x2, y2)
plt.yscale('linear')
plt.title('2 MS')
plt.grid(True)



y3 = np.random.exponential( scale=0.1825, size=1000)
y3 = y3[(y3 > 0) & (y3 < 2)]
y3.sort()
x3 = np.arange(len(y3))
# plot with various axes scales
#plt.figure("3 MS")
# linear
plt.subplot(223)
plt.plot(x3, y3)
plt.yscale('linear')
plt.title('3 MS')
plt.grid(True)


y4 = np.random.exponential( scale=0.09125, size=1000)
y4 = y4[(y4 > 0) & (y4 < 2)]
y4.sort()
x4 = np.arange(len(y4))
# plot with various axes scales
#plt.figure("4 MS")
# linear
plt.subplot(224)
plt.plot(x3, y3)
plt.yscale('linear')
plt.title('4 MS')
plt.grid(True)


y5 = np.random.exponential( scale=7.34, size=100000)
y5 = y5[(y5 > 0) & (y5 < 30)]
y5.sort()
x5 = np.arange(len(y5))
# plot with various axes scales
plt.figure("2 MS")
# linear
#plt.subplot(222)
plt.plot(x5, y5)
plt.yscale('linear')
plt.title('2 MS')
plt.grid(True)


y5 = np.random.exponential( scale=7.34, size=100000)
y5 = y5[(y5 > 0) & (y5 < 30)]
y5.sort()
x5 = np.arange(len(y5))
# plot with various axes scales
plt.figure("2 MS")
# linear
#plt.subplot(222)
plt.plot(x5, y5)
plt.yscale('linear')
plt.title('2 MS')
plt.grid(True)


y5 = np.random.exponential( scale=7.34, size=100000)
y5 = y5[(y5 > 0) & (y5 < 30)]
y5.sort()
x5 = np.arange(len(y5))
# plot with various axes scales
plt.figure("2 MS")
# linear
#plt.subplot(222)
plt.plot(x5, y5)
plt.yscale('linear')
plt.title('2 MS')
plt.grid(True)

plt.show()


