import numpy as np
import matplotlib.pyplot as plt

from matplotlib.ticker import NullFormatter  # useful for `logit` scale

# Fixing random state for reproducibility

# make up some data in the interval ]0, 1[
y = np.random.exponential( scale=0.734, size=1000)
y = y[(y > 0) & (y < 2)]
y.sort()
x = np.arange(len(y))


# plot with various axes scales
plt.figure("1 MS")

# linear
#plt.subplot(221)
plt.plot(x, y)
plt.yscale('linear')
plt.title('Rho Diagram')
plt.grid(True)


y5 = np.random.exponential( scale=7.34, size=1000)
y5 = y5[(y5 > 0) & (y5 < 30)]
y5.sort()
x5 = np.arange(len(y5))
# plot with various axes scales
plt.figure("2 MS")
# linear
#plt.subplot(222)
plt.plot(x5, y5)
plt.yscale('linear')
plt.title('Number in the System L')
plt.grid(True)

plt.show()