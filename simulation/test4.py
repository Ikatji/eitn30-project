import simpy


def packet(env, name, bs, sending_time, servive_time):
    # Simulate arrive to the BS
    yield env.timeout(sending_time)

    # Take place in BS and start processing / Waite if there is no place!!
    print('%s arriving at %d' % (name, env.now))
    with bs.request() as req:
        yield req

        # start responce
        print('%s starting to responce at %s' % (name, env.now))
        yield env.timeout(servive_time)
        print('%s send ACK from BS at %s' % (name, env.now))


env = simpy.Environment()
BS = simpy.Resource(env, capacity=5) # service 2 packets at the same time 
for i in range(100):
    env.process(packet(env, 'Packet %d' % i, BS, i*2, 3))
env.run()