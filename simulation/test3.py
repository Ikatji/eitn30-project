import simpy

class packet(object):
    def __init__(self, env):
        self.env = env
     # Start the run process everytime an instance is created.
        self.action = env.process(self.run())

    def run(self):
       while True:
            print('Start reciving and sending ack at %d' % self.env.now)
            send_time_duration = 5
            # We may get interrupted while sending ack 
            try:
                yield self.env.process(self.send_time(send_time_duration))
            except simpy.Interrupt:
                # When we received an interrupt, we stop sending ack and
                # switch to the "reciving" state
                print('Was interrupted.  No ack has been send!!')

            print('Start sendig data at %d' % self.env.now)
            trip_duration = 2
            yield self.env.timeout(trip_duration)

    def send_time(self, duration):
        yield self.env.timeout(duration)
   

def send_anyway(env, packet):
    yield env.timeout(3)
    packet.action.interrupt()

env = simpy.Environment()
packet = packet(env)
env.process(send_anyway(env, packet))
env.run(until=1000)